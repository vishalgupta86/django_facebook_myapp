from django.shortcuts import render
from courses.models import Course

def home(request):
    if request.facebook: 
        me = request.facebook.graph.get_object('me')     
	is_logged_in = True
        top_courses = Course.objects.all()[:10]
        return render(request, 'home.html', {'me' : me, 'courses' : top_courses, 'is_logged_in' : is_logged_in})
    else:
	is_logged_in = False
        return render(request, 'home.html', {'is_logged_in' : is_logged_in})	
